from random import randrange, choice
from time import sleep


def display_board(board):  # parametr board
    #
    # Funkcja, która przyjmuje jeden parametr zawierający bieżący stan tablicy
    # i wyświetla go w oknie konsoli.
    #
    print("+-------+-------+-------+", )
    print("|       |       |       |\n")
    print("|  ", board[0][0], "  |  ", board[0][1], "  |  ", board[0][2], "  |\n")
    print("|       |       |       |\n")
    print("+-------+-------+-------+", )
    print("|       |       |       |\n")
    print("|  ", board[1][0], "  |  ", board[1][1], "  |  ", board[1][2], "  |\n")
    print("|       |       |       |\n")
    print("+-------+-------+-------+", )
    print("|       |       |       |\n")
    print("|  ", board[2][0], "  |  ", board[2][1], "  |  ", board[2][2], "  |\n")
    print("|       |       |       |\n")
    print("+-------+-------+-------+", )


def enter_move(board):
    #
    # Funkcja, która przyjmuje parametr odzwierciedlający biężący stan tablicy,
    # prosi użytkownika o wykonanie ruchu,
    # sprawdza dane wejściowe i aktualizuje tablicę zgodnie z decyzją użytkownika.
    #
    wolne_pola = make_list_of_free_fields(board)
    field = input("Podaj numer pola na którym chesz postawić kółko: ")
    # tu wprowadzic sprawdzanie czy pole jest wolne

    if field == "1":
        if (0, 0) in wolne_pola:
            board[0][0] = "O"
        else:
            print("To pole jest zajęte")
            enter_move(board)
    elif field == "2":
        if (0, 1) in wolne_pola:
            board[0][1] = "O"
        else:
            print("To pole jest zajęte")
            enter_move(board)
    elif field == "3":
        if (0, 2) in wolne_pola:
            board[0][2] = "O"
        else:
            print("To pole jest zajęte")
            enter_move(board)
    elif field == "4":
        if (1, 0) in wolne_pola:
            board[1][0] = "O"
        else:
            print("To pole jest zajęte")
            enter_move(board)
    elif field == "5":
        if (1, 1) in wolne_pola:
            board[1][1] = "O"
        else:
            print("To pole jest zajęte")
            enter_move(board)
    elif field == "6":
        if (1, 2) in wolne_pola:
            board[1][2] = "O"
        else:
            print("To pole jest zajęte")
            enter_move(board)
    elif field == "7":
        if (2, 0) in wolne_pola:
            board[2][0] = "O"
        else:
            print("To pole jest zajęte")
            enter_move(board)
    elif field == "8":
        if (2, 1) in wolne_pola:
            board[2][1] = "O"
        else:
            print("To pole jest zajęte")
            enter_move(board)
    elif field == "9":
        if (2, 2) in wolne_pola:
            board[2][2] = "O"
        else:
            print("To pole jest zajęte")
            enter_move(board)
    else:
        print("Takie pole nie istnieje")
        enter_move(board)


#############################

def make_list_of_free_fields(board):
    #
    # Funkcja, która przegląda tablicę i tworzy listę wszystkich wolnych pól;
    # lista składa się z krotek, a każda krotka zawiera parę liczb odzwierciedlających rząd i kolumnę.
    #

    # sprawdzic czy istnieje metoda do przeglądania zawartości i zwracania pozycji na liscie
    wolne_pola = []

    for rzad in range(0, 3):
        for kolumna in range(0, 3):
            if board[rzad][kolumna] == "O" or board[rzad][kolumna] == "x":
                continue
            else:
                wolne_pola.append((rzad, kolumna))
    # print("lista wolnych pól:",wolne_pola)
    return wolne_pola


##############################

def victory_for(board):  # był jeszcze sign
    #
    # Funkcja, która dokonuje analizy stanu tablicy w celu sprawdzenia
    # czy użytkownik/gracz stosujący "O" lub "X" wygrał rozgrywkę.
    #
    global KONIEC
    # print("Board in victory: ", board)

    # GRACZ

    # wiersze

    if board[0][0] == "O" and board[0][1] == "O" and board[0][2] == "O":
        KONIEC = 0
        return print("Wygrałeś")
    if board[1][0] == "O" and board[1][1] == "O" and board[1][2] == "O":
        KONIEC = 0
        return print("Wygrałeś")
    if board[2][0] == "O" and board[2][1] == "O" and board[2][2] == "O":
        KONIEC = 0
        return print("Wygrałeś")
    # kolumny
    if board[0][0] == "O" and board[1][0] == "O" and board[2][0] == "O":
        KONIEC = 0
        return print("Wygrałeś")
    if board[0][1] == "O" and board[1][1] == "O" and board[2][1] == "O":
        KONIEC = 0
        return print("Wygrałeś")
    if board[0][2] == "O" and board[1][2] == "O" and board[2][2] == "O":
        KONIEC = 0
        return print("Wygrałeś")
    # ukosy
    if board[0][0] == "O" and board[1][1] == "O" and board[2][2] == "O":
        KONIEC = 0
        return print("Wygrałeś")
    if board[0][2] == "O" and board[1][1] == "O" and board[2][0] == "O":
        KONIEC = 0
        return print("Wygrałeś")

    # KOMPUTER
    # wiersze
    if board[0][0] == "x" and board[0][1] == "x" and board[0][2] == "x":
        KONIEC = 0
        return print("Wygrał komputer")
    if board[1][0] == "x" and board[1][1] == "x" and board[1][2] == "x":
        KONIEC = 0
        return print("Wygrał komputer")
    if board[2][0] == "x" and board[2][1] == "x" and board[2][2] == "x":
        KONIEC = 0
        return print("Wygrał komputer")
    # kolumny
    if board[0][0] == "x" and board[1][0] == "x" and board[2][0] == "x":
        KONIEC = 0
        return print("Wygrał komputer")
    if board[0][1] == "x" and board[1][1] == "x" and board[2][1] == "x":
        KONIEC = 0
        return print("Wygrał komputer")
    if board[0][2] == "x" and board[1][2] == "x" and board[2][2] == "x":
        KONIEC = 0
        return print("Wygrał komputer")
    # ukosy
    if board[0][0] == "x" and board[1][1] == "x" and board[2][2] == "x":
        KONIEC = 0
        return print("Wygrał komputer")
    if board[0][2] == "x" and board[1][1] == "x" and board[2][0] == "x":
        KONIEC = 0
        return print("Wygrał komputer")

    return print("Gramy dalej")


########################

def draw_move(board, difficulty):
    #
    # Funkcja, która wykonuje ruch za komputer i aktualizuje tablicę.
    #
    first_move = 0
    print("Komputer wykonuje swój ruch")
    wolne_pola = make_list_of_free_fields(board)
    # print("Wolne pola dla ruchu komputera: ", wolne_pola)
    # print("BOARD: ", board)
    Tboard = [list(i) for i in zip(*board)]  # transpose

    if first_move == 0 or difficulty == 1:
        field = randrange(1, 10)
        first_move = 1

    if first_move != 0 and difficulty == 2:

        if "x" in board[0] and "O" not in board[0]:
            field = choice([1, 2, 3])
        elif "x" in board[1] and "O" not in board[1]:
            field = choice([4, 5, 6])
        elif "x" in board[2] and "O" not in board[2]:
            field = choice([7, 8, 9])
        elif "x" in Tboard[0] and "O" not in Tboard[0]:
            field = choice([1, 4, 7])
        elif "x" in Tboard[1] and "O" not in Tboard[1]:
            field = choice([2, 5, 8])
        elif "x" in Tboard[2] and "O" not in Tboard[2]:
            field = choice([3, 6, 9])
        else:
            field = randrange(1, 10)
    print("Pole jakie wybrał komputer", field)
    if field == 1:
        if (0, 0) in wolne_pola:
            board[0][0] = "x"
        else:
            draw_move(board, difficulty)
    elif field == 2:
        if (0, 1) in wolne_pola:
            board[0][1] = "x"
        else:
            draw_move(board, difficulty)
    elif field == 3:
        if (0, 2) in wolne_pola:
            board[0][2] = "x"
        else:
            draw_move(board, difficulty)
    elif field == 4:
        if (1, 0) in wolne_pola:
            board[1][0] = "x"
        else:
            draw_move(board, difficulty)
    elif field == 5:
        if (1, 1) in wolne_pola:
            board[1][1] = "x"
        else:
            draw_move(board, difficulty)
    elif field == 6:
        if (1, 2) in wolne_pola:
            board[1][2] = "x"
        else:
            draw_move(board, difficulty)
    elif field == 7:
        if (2, 0) in wolne_pola:
            board[2][0] = "x"
        else:
            draw_move(board, difficulty)
    elif field == 8:
        if (2, 1) in wolne_pola:
            board[2][1] = "x"
        else:
            draw_move(board, difficulty)
    elif field == 9:
        if (2, 2) in wolne_pola:
            board[2][2] = "x"
        else:
            draw_move(board, difficulty)


########################

# wolne_pola = []
KONIEC = 1
print(KONIEC)
# board = [[1,2,3],[4,5,6],[7,8,9]]
board = [['1', '2', '3'], ['4', '5', '6'], ['7', '8', '9']]
time = 3
wypelnienie = 0

print("Oto pole gry. Komputer stawia X a gracz O\n")
difficulty = int(input("Wybierz poziom trudności. 1 lub 2 \n"))
while difficulty not in [1, 2]:
    print("Zły wybór")
    difficulty = int(input("Wybierz poziom trudności. 1 lub 2 \n"))
display_board(board)
print("Zaczynamy grę za: ")
while time > -1:
    print(time)
    sleep(1)
    time -= 1

while KONIEC:
    draw_move(board, difficulty)
    display_board(board)
    victory_for(board)
    if KONIEC == 0:
        break
    for i in range(len(board)):  # dorobic sprawdzanie remisu
        for j in board[i]:
            if not j.isdigit():
                wypelnienie += 1
    if wypelnienie == 9:
        print("REMIS")
        break
    wypelnienie = 0
    enter_move(board)
    display_board(board)
    victory_for(board)

print("KONIEC GRY")
input("Wcisnij klawisz aby zakończyć")